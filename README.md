# Scan Me App

Scan Me App is an app to identify a math expression in text by using built-in camera in your device.
It can help you to calculate a simple math operator.


## Getting started

To build this project there are 4 build flavors that you can choose: 
* Red File System (Scan the text by picked image from your device gallery )
* Red Built-in Camera (Scan the text by your device camera)
* Green File System (Scan the text by picked image from your device gallery )
* Green Built-in Camera (Scan the text by your device camera)



## Architecture
This app is created based on recommended google arhictecture component and Clean Architecture:

- Clean Architecture (data, domain, ui)
- Jetpack compose
- Navigation compose
- MLKit (Image Recognizer)
- Dagger Hilt
- Courountine
- MVVM + LiveData
- Room 


## License
[MIT License](LICENSE)
