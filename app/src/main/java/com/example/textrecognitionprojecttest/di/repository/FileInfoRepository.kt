package com.example.textrecognitionprojecttest.di.repository

import com.example.textrecognitionprojecttest.data.local.entities.FileInfo

interface FileInfoRepository {
    suspend fun getFileInfo() : List<FileInfo>

    suspend fun insertFileInfo(fileInfo: FileInfo)
}