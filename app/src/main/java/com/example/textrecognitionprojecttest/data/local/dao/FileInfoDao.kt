package com.example.textrecognitionprojecttest.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.example.textrecognitionprojecttest.data.local.entities.FileInfo

@Dao
interface FileInfoDao {
    @Query("SELECT * FROM file_info ORDER BY id DESC")
    suspend fun getFiles(): List<FileInfo>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFile(fileInfo: FileInfo)

    @Update
    suspend fun updateFile(fileInfo: FileInfo)

    @Query("DELETE FROM file_info WHERE id = :id")
    suspend fun deleteFile(id: Int)
}