package com.example.textrecognitionprojecttest.data.local.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "file_info")
data class FileInfo(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,

    val input: String,
    val result: String,

    @ColumnInfo(name = "file_name")
    val fileName: String
)