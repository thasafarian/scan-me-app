package com.example.textrecognitionprojecttest.data.repositories

import com.example.textrecognitionprojecttest.data.local.database.AppDatabase
import com.example.textrecognitionprojecttest.data.local.entities.FileInfo
import com.example.textrecognitionprojecttest.di.repository.FileInfoRepository
import javax.inject.Inject

class FileInfoRepositoryImpl @Inject constructor(
    private val appDatabase: AppDatabase
) : FileInfoRepository{

    override suspend fun getFileInfo(): List<FileInfo> {
        return appDatabase.fileInfoDao().getFiles()
    }

    override suspend fun insertFileInfo(fileInfo: FileInfo) {
        return appDatabase.fileInfoDao().insertFile(fileInfo)
    }
}