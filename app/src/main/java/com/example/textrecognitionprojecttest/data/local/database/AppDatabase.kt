package com.example.textrecognitionprojecttest.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.textrecognitionprojecttest.data.local.dao.FileInfoDao
import com.example.textrecognitionprojecttest.data.local.entities.FileInfo

@Database(entities = [FileInfo::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun fileInfoDao(): FileInfoDao
}