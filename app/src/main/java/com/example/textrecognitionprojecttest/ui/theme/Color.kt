package com.example.textrecognitionprojecttest.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val Red200 = Color(0xFFF04780)
val Red500 = Color(0xFFCC325E)
val Red700 = Color(0xFFA52852)
val Green200 = Color(0xFF8FE940)
val Green500 = Color(0xFF72C247)
val Green700 = Color(0xFF31AA23)