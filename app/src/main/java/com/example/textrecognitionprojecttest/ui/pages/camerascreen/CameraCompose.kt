package com.example.textrecognitionprojecttest.ui.pages.camerascreen

import android.Manifest
import android.content.pm.PackageManager
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.text.ClickableText
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import com.example.textrecognitionprojecttest.ui.Route
import com.example.textrecognitionprojecttest.utils.Commons.REQUIRED_PERMISSIONS
import com.example.textrecognitionprojecttest.utils.CameraUtils
import com.example.textrecognitionprojecttest.utils.Commons.showToast

@Composable
fun CameraCompose(
    navController: NavController,
    cameraX: CameraUtils,
    content: @Composable() (ColumnScope.() -> Unit),
) {
    val context = LocalContext.current
    var hasCamPermission by remember {
        mutableStateOf(
            REQUIRED_PERMISSIONS.all {
                ContextCompat.checkSelfPermission(context, it) ==
                        PackageManager.PERMISSION_GRANTED
            })
    }


    val launcher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestMultiplePermissions(),
        onResult = { granted ->
            hasCamPermission = granted.size == 2
        }
    )

    LaunchedEffect(key1 = true) {
        launcher.launch(
            arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        )
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        if (hasCamPermission) {
            AndroidView(
                modifier = Modifier.fillMaxSize(),
                factory = { cameraX.startCameraPreviewView() }
            )

            BoxScanner(navController)
        } else {
            showToast(context, "Camera has no permission")
        }
    }
    Column(
        modifier = Modifier.fillMaxSize(), Arrangement.Bottom, Alignment.CenterHorizontally,
        content = content
    )
}


@Composable
fun BoxScanner(
    navController: NavController
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth(0.5f)
                .fillMaxHeight(0.3f)
                .background(Color.Transparent)
                .border(
                    width = 2.dp,
                    color = Color.Yellow,
                    shape = RectangleShape
                )
        )

        Spacer(modifier = Modifier.height(15.dp))

        ClickableText(
            text = AnnotatedString("Make sure you capture a Math Expression in the rectangle box"),
            style = TextStyle(
                fontSize = 25.sp,
                color = Color.White
            ),
            onClick = {
                navController.navigate(Route.PICKER_SCREEN)
            }
        )
    }
}



