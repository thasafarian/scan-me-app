package com.example.textrecognitionprojecttest.ui.theme


import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import com.example.textrecognitionprojecttest.BuildConfig

private val DarkColorPalette = darkColors(
    primary = Purple200,
    primaryVariant = Purple700,
    secondary = Teal200
)

private val LightColorPalette = lightColors(
    primary = Purple500,
    primaryVariant = Purple700,
    secondary = Teal200
)


private val RedColorPalette = lightColors(
    primary = Red500,
    primaryVariant = Red700,
    secondary = Red200
)

private val GreenColorPalette = lightColors(
    primary = Green200,
    primaryVariant = Green500,
    secondary = Green700
)

@Composable
fun CameraAppTheme(darkTheme: Boolean = isSystemInDarkTheme(),
                   content: @Composable () -> Unit) {
    var colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    if (BuildConfig.FLAVOR_NAME.contains("red")) {
        colors = RedColorPalette
    } else if (BuildConfig.FLAVOR_NAME.contains("green")) {
        colors = GreenColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}