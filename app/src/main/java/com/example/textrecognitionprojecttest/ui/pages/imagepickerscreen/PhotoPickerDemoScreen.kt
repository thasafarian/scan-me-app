package com.example.textrecognitionprojecttest.ui.pages.imagepickerscreen

import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.example.textrecognitionprojecttest.ui.Route
import com.example.textrecognitionprojecttest.ui.pages.camerascreen.FileInfoViewModel
import com.example.textrecognitionprojecttest.utils.CameraUtils
import com.example.textrecognitionprojecttest.utils.Commons
import com.example.textrecognitionprojecttest.utils.getBitmapFromUri
import com.example.textrecognitionprojecttest.utils.processTheImageWithRecognizerKit

@Composable
fun PhotoPickerDemoScreen(
    navController: NavController,
    fileInfoViewModel: FileInfoViewModel,
    owner: LifecycleOwner
) {
    val context = LocalContext.current
    var photoUri: Uri? by remember { mutableStateOf(null) }
    val launcher =
        rememberLauncherForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
            photoUri = uri
        }

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize()
    ) {
        Button(
            onClick = {
                launcher.launch(
                    PickVisualMediaRequest(
                        mediaType = ActivityResultContracts.PickVisualMedia.ImageOnly
                    )
                )
            }
        ) {
            Text("Select Photo")
        }

        if (photoUri != null) {
            //Use Coil to display the selected image
            val painter = rememberAsyncImagePainter(
                ImageRequest
                    .Builder(LocalContext.current)
                    .data(data = photoUri)
                    .build()
            )

            Image(
                painter = painter,
                contentDescription = null,
                modifier = Modifier
                    .padding(5.dp)
                    .fillMaxWidth()
                    .border(6.0.dp, Color.Gray),
                contentScale = ContentScale.Crop
            )


            LaunchedEffect(Unit) {
                val bitmap = getBitmapFromUri(context.contentResolver, photoUri!!)

                processTheImageWithRecognizerKit(
                    context = context,
                    fileInfoViewModel = fileInfoViewModel,
                    bitmap = bitmap!!,
                    owner = owner,
                    cameraCalculationListener = object : CameraUtils.CameraCalculationListener {
                        override fun onSuccessCalculation() {
                            navController.navigate(Route.RESULT_SCREEN)
                        }

                        override fun onFailedCalculation(message: String?) {
                            Commons.showToast(
                                context,
                                message ?: "An error occurred while calculating data"
                            )
                        }

                    }
                )
            }
        }
    }
}