package com.example.textrecognitionprojecttest.ui.pages.filescreen

import android.content.ContentResolver
import android.widget.Space
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.textrecognitionprojecttest.data.local.entities.FileInfo
import com.example.textrecognitionprojecttest.utils.getImagesFromPicturesDirectory


@Composable
fun FileItem(data: FileInfo) {

    val contentResolver: ContentResolver = LocalContext.current.contentResolver
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
    ) {
       /* Image(
            bitmap = getImagesFromPicturesDirectory(contentResolver, data)!!,
            contentDescription = data.fileName,
            modifier = Modifier
                .size(120.dp)
                .clip(shape = RoundedCornerShape(8.dp))
        )*/

        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp),
            elevation = 8.dp,
            backgroundColor = MaterialTheme.colors.surface,
        ) {
            Column(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = "${data.input} = ${data.result}",
                    style = TextStyle(
                        fontSize = 25.sp,
                        color = Color.DarkGray,
                        fontFamily = FontFamily.SansSerif
                    ),
                    color = MaterialTheme.colors.onSurface
                )
            }
        }
    }


}