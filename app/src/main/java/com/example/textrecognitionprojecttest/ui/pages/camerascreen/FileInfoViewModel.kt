package com.example.textrecognitionprojecttest.ui.pages.camerascreen

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.textrecognitionprojecttest.data.local.entities.FileInfo
import com.example.textrecognitionprojecttest.domain.DataState
import com.example.textrecognitionprojecttest.domain.GetFilesInfoUseCase
import com.example.textrecognitionprojecttest.domain.InsertFileUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class FileInfoViewModel @Inject constructor(
    private val getFilesInfoUseCase: GetFilesInfoUseCase,
    private val insertFileUseCase: InsertFileUseCase
) : ViewModel() {

    private val _fileInfoState = mutableStateOf<DataState<List<FileInfo>>>(DataState.Loading)
    val fileInfoState: State<DataState<List<FileInfo>>> = _fileInfoState

    fun getFileInfoList() {
        viewModelScope.launch {
            try {
                val result = async { getFilesInfoUseCase.invoke() }
                _fileInfoState.value = DataState.Success(result.await())
            } catch (e: Exception) {
                _fileInfoState.value = DataState.Error("Sorry, the files cannot be retrieved")
            }
        }
    }

    fun saveFile(input: String, result: String, fileName: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val data = FileInfo(
                input = input,
                result = result,
                fileName = fileName
            )
            insertFileUseCase.invoke(data)
        }
    }

}