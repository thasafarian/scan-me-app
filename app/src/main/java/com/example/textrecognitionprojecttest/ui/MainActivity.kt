package com.example.textrecognitionprojecttest.ui

import SplashScreen
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.Surface
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.textrecognitionprojecttest.ui.Route.MAIN_SCREEN
import com.example.textrecognitionprojecttest.ui.Route.PICKER_SCREEN
import com.example.textrecognitionprojecttest.ui.Route.RESULT_SCREEN
import com.example.textrecognitionprojecttest.ui.Route.SPLASH_SCREEN
import com.example.textrecognitionprojecttest.ui.Route.TEST_SCREEN
import com.example.textrecognitionprojecttest.ui.pages.camerascreen.CameraResultScreen
import com.example.textrecognitionprojecttest.ui.pages.camerascreen.CameraScreen
import com.example.textrecognitionprojecttest.utils.CameraUtils
import com.example.textrecognitionprojecttest.ui.pages.camerascreen.FileInfoViewModel
import com.example.textrecognitionprojecttest.ui.pages.imagepickerscreen.PhotoPickerDemoScreen
import com.example.textrecognitionprojecttest.ui.pages.test.ScreenTest
import com.example.textrecognitionprojecttest.ui.theme.CameraAppTheme
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private var cameraX: CameraUtils = CameraUtils(this, this)
    private val fileInfoViewModel: FileInfoViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Surface {
                CameraAppTheme {


                    val navController = rememberNavController()
                    NavHost(navController = navController, startDestination = SPLASH_SCREEN) {
                        composable(SPLASH_SCREEN) {
                            SplashScreen(navController = navController)
                        }

                        composable(MAIN_SCREEN) {
                            CameraScreen(
                                navController,
                                fileInfoViewModel,
                                cameraX
                            )
                        }

                        composable(TEST_SCREEN) { ScreenTest(navController) }

                        composable(RESULT_SCREEN) { backStackEntry ->
                            backStackEntry.updateState()
                            CameraResultScreen(
                                navController = navController,
                                fileInfoViewModel = fileInfoViewModel,
                            )
                        }

                        composable(PICKER_SCREEN) {
                            PhotoPickerDemoScreen(
                                navController = navController,
                                fileInfoViewModel = fileInfoViewModel,
                                owner = this@MainActivity
                            )
                        }
                    }
                }
            }
        }
    }
}


object Route {
    const val SPLASH_SCREEN = "splash_screen"
    const val MAIN_SCREEN = "main_screen"
    const val TEST_SCREEN = "test_screen"
    const val RESULT_SCREEN = "result_screen/{input_expression}/{result}"
    const val PICKER_SCREEN = "picker_screen"
}