package com.example.textrecognitionprojecttest.ui.pages.camerascreen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.textrecognitionprojecttest.domain.DataState
import com.example.textrecognitionprojecttest.ui.Route
import com.example.textrecognitionprojecttest.ui.pages.filescreen.FileItem
import com.example.textrecognitionprojecttest.utils.Commons.showToast

@Composable
fun CameraResultScreen(
    navController: NavController,
    fileInfoViewModel: FileInfoViewModel
) {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        val context = LocalContext.current
        val dataState by fileInfoViewModel.fileInfoState

        when (dataState) {
            is DataState.Loading -> {
                CircularProgressIndicator()
            }

            is DataState.Success -> {
                val data = (dataState as DataState.Success).data
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .weight(1f)
                ) {
                    LazyColumn {
                        items(data) {
                            FileItem(data = it)
                        }
                    }
                }
            }

            is DataState.Error -> {
                val errorMessage = (dataState as DataState.Error).message
                showToast(
                    context = context,
                    msg = errorMessage
                )
            }
        }

        Spacer(modifier = Modifier.height(12.dp))

        Button(onClick = { navController.navigate(Route.MAIN_SCREEN) }) {
            Text(
                text = "Scan Again", style = TextStyle(
                    fontSize = 23.sp,
                    fontFamily = FontFamily.Monospace
                )
            )
        }
    }

    LaunchedEffect(Unit) {
        fileInfoViewModel.getFileInfoList()
    }
}