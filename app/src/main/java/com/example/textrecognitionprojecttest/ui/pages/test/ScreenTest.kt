package com.example.textrecognitionprojecttest.ui.pages.test

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.OffsetMapping
import androidx.compose.ui.text.input.TransformedText
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.textrecognitionprojecttest.utils.validateMathExpression

@Composable
fun ScreenTest(navController: NavHostController) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize()
    ) {
        var text: String by remember { mutableStateOf("") }
        TextField(
            value = text,
            onValueChange = { newText -> text = newText },
            label = { Text("Enter math expression here") }
        )

        Spacer(modifier = Modifier.height(10.dp))

        Button(onClick = { /*validateMathExpression(text)*/ }) {
            Text(text = "Calculate Now")
        }

        Spacer(modifier = Modifier.height(10.dp))

        Text("The Result: validateMathExpression(text)")
    }
}
