package com.example.textrecognitionprojecttest.ui.pages.camerascreen

import android.content.Context
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.textrecognitionprojecttest.ui.Route
import com.example.textrecognitionprojecttest.utils.CameraUtils
import com.example.textrecognitionprojecttest.utils.Commons

@Composable
fun CameraScreen(
    navController: NavController,
    fileInfoViewModel: FileInfoViewModel,
    cameraX: CameraUtils
) {
    val context = LocalContext.current
    CameraCompose(
        navController = navController,
        cameraX = cameraX
    ) {
        Button(
            shape = RoundedCornerShape(16.dp),
            onClick = {
                startCapturingImage(
                    navController = navController,
                    fileInfoViewModel = fileInfoViewModel,
                    cameraX = cameraX,
                    context = context
                )
            }
        ) {
            Text(
                modifier = Modifier.padding(4.dp),
                text = "Capture",
                style = TextStyle(
                    fontSize = 25.sp
                ),
            )
        }
    }
}

private fun startCapturingImage(
    navController: NavController,
    fileInfoViewModel: FileInfoViewModel,
    cameraX: CameraUtils,
    context: Context
) {
    if (Commons.allPermissionsGranted(context)) {
        cameraX.capturePhoto(
            fileInfoViewModel, object : CameraUtils.CameraCalculationListener {
                override fun onSuccessCalculation() {
                    // navigate to compose
                    navController.navigate(Route.RESULT_SCREEN)
                }

                override fun onFailedCalculation(message: String?) {
                    Commons.showToast(context, message ?: "")
                }
            })

    } else {
        Commons.showToast(
            context = context,
            msg = "Permission is not granted, please check permission on your device"
        )
    }
}