package com.example.textrecognitionprojecttest.domain

import com.example.textrecognitionprojecttest.data.local.entities.FileInfo
import com.example.textrecognitionprojecttest.data.repositories.FileInfoRepositoryImpl
import javax.inject.Inject

class GetFilesInfoUseCase @Inject constructor(private val fileInfoRepository: FileInfoRepositoryImpl) {

    suspend fun invoke(): List<FileInfo> {
        return fileInfoRepository.getFileInfo()
    }
}
