package com.example.textrecognitionprojecttest.utils

import android.content.Context
import android.graphics.Bitmap
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.example.textrecognitionprojecttest.ui.pages.camerascreen.FileInfoViewModel
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import kotlinx.coroutines.launch

val recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)

suspend fun processTheImageWithRecognizerKit(
    context: Context,
    fileInfoViewModel: FileInfoViewModel,
    bitmap: Bitmap,
    cameraCalculationListener: CameraUtils.CameraCalculationListener,
    owner: LifecycleOwner

) {
    // create file name
    val fileName = "${System.currentTimeMillis()}.jpg"

    // save image to media files
    saveImageToMediaStore(
        context = context,
        fileName = fileName,
        bitmap = bitmap
    )

    // proceed the image recognizer
    val inputImage = InputImage.fromBitmap(bitmap, 0)
    recognizer.process(inputImage).addOnSuccessListener { visionText ->
        owner.lifecycleScope.launch {
            startCalculateTheMathExpression(
                cameraCalculationListener = cameraCalculationListener,
                context = context,
                fileInfoViewModel = fileInfoViewModel,
                mathExpression = visionText.text,
                fileName = fileName
            )
        }
    }.addOnFailureListener { e ->
        cameraCalculationListener.onFailedCalculation(e.message)
    }
}

private suspend fun startCalculateTheMathExpression(
    cameraCalculationListener: CameraUtils.CameraCalculationListener,
    context: Context,
    fileInfoViewModel: FileInfoViewModel,
    mathExpression: String,
    fileName: String
) {
    if (isMathExpressionIsValid(mathExpression)) {
        val inputExpression = getInputExpression(mathExpression)
        val result = validateMathExpression(mathExpression)

        Commons.showToast(
            context = context,
            msg = "Math expression is available!"
        )

        // save file to database
        fileInfoViewModel.saveFile(
            input = inputExpression,
            result = result.toString(),
            fileName = fileName
        )
        cameraCalculationListener.onSuccessCalculation()
    } else {
        cameraCalculationListener.onFailedCalculation("No math expression available")
    }
}

private suspend fun saveImageToMediaStore(
    context: Context,
    fileName: String,
    bitmap: Bitmap
) {
    val mimeType = "image/jpeg"
    saveImageToMediaStore(
        context = context,
        displayName = fileName,
        mimeType = mimeType,
        image = bitmap,
    )
}