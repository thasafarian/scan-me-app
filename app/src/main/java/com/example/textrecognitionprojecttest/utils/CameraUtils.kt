package com.example.textrecognitionprojecttest.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture

import androidx.camera.core.ImageCaptureException
import androidx.camera.core.ImageProxy
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.example.textrecognitionprojecttest.ui.pages.camerascreen.FileInfoViewModel
import com.example.textrecognitionprojecttest.utils.Commons.showLog
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.nio.ByteBuffer


class CameraUtils(
    private var context: Context,
    private var owner: LifecycleOwner,
) {
    private var imageCapture: ImageCapture? = null

    // When using Latin script library
    private var cameraCalculationListener_: CameraCalculationListener? = null

    interface CameraCalculationListener {
        fun onSuccessCalculation()
        fun onFailedCalculation(message: String?)
    }

    fun startCameraPreviewView(): PreviewView {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(context)
        val previewView = PreviewView(context)
        val preview = Preview.Builder().build().also {
            it.setSurfaceProvider(previewView.surfaceProvider)
        }

        imageCapture = ImageCapture.Builder().build()

        val camSelector =
            CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_BACK).build()
        try {
            cameraProviderFuture.get().bindToLifecycle(
                owner,
                camSelector,
                preview,
                imageCapture
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return previewView
    }

    fun capturePhoto(
        fileInfoViewModel: FileInfoViewModel,
        cameraCalculationListener: CameraCalculationListener
    ) = owner.lifecycleScope.launch {
            val imageCapture = imageCapture ?: return@launch
            cameraCalculationListener_ = cameraCalculationListener

            imageCapture.takePicture(ContextCompat.getMainExecutor(context), object :
                ImageCapture.OnImageCapturedCallback(), ImageCapture.OnImageSavedCallback {
                override fun onCaptureSuccess(image: ImageProxy) {
                    super.onCaptureSuccess(image)
                    owner.lifecycleScope.launch {
                        val bitmap = imageProxyToBitmap(image)
                        // process the image here
                        processTheImageWithRecognizerKit(
                            context = context,
                            fileInfoViewModel = fileInfoViewModel,
                            bitmap = bitmap,
                            cameraCalculationListener = cameraCalculationListener,
                            owner = owner
                        )
                    }
                }

                override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
                    showLog("onCaptureSuccess: Uri  ${outputFileResults.savedUri}")
                }

                override fun onError(exception: ImageCaptureException) {
                    super.onError(exception)
                    showLog("onCaptureSuccess: onError")
                }
            })
        }

    private suspend fun imageProxyToBitmap(image: ImageProxy): Bitmap =
        withContext(owner.lifecycleScope.coroutineContext) {
            val planeProxy = image.planes[0]
            val buffer: ByteBuffer = planeProxy.buffer
            val bytes = ByteArray(buffer.remaining())
            buffer.get(bytes)
            BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
        }
}