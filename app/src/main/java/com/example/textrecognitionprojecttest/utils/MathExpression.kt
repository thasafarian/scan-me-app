package com.example.textrecognitionprojecttest.utils

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import net.objecthunter.exp4j.ExpressionBuilder

suspend fun isMathExpressionIsValid(text: String): Boolean {
    val result = withContext(Dispatchers.IO) {
        async {
            validateMathExpression(text) != 0.0
        }
    }
   return result.await()
}

fun validateMathExpression(expression: String): Double {
    val newText = textValidation(expression)
    return try {
        val result = ExpressionBuilder(
            extractMathExpressions(newText)[0]
        ).build().evaluate()
        result
    } catch (e: Exception) {
        0.0
    }
}

private fun textValidation(text: String): String {
    text.replace(Regex("[:]"), "/")
    text.replace(Regex("[x×]"), "*")
    return text
}

private fun extractMathExpressions(text: String): List<String> {
    val expressions = Regex("\\d+\\s*[-+*/]\\s*\\d+").findAll(text)
        .map { it.value.trim() }
        .toList()

    expressions.forEach { expression ->
        println(expression)
    }
    return expressions
}

// Only the first expression that will be fetched
fun getInputExpression(text: String): String {
    return textValidation(
        extractMathExpressions(text)[0]
    )
}