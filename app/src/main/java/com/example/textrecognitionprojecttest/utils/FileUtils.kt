package com.example.textrecognitionprojecttest.utils

import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.widget.Toast
import androidx.camera.core.ImageProxy
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import com.example.textrecognitionprojecttest.data.local.entities.FileInfo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.io.IOException
import java.io.OutputStream
import java.security.AccessController.getContext


suspend fun saveImageToMediaStore(
    context: Context,
    displayName: String,
    mimeType: String,
    image: Bitmap
): Boolean {
    return withContext(Dispatchers.IO) {
        val imageCollectionUri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
        } else {
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        }

        val contentValues = ContentValues().apply {
            put(MediaStore.Images.Media.DISPLAY_NAME, displayName)
            put(MediaStore.Images.Media.MIME_TYPE, mimeType)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                put(MediaStore.Images.Media.IS_PENDING, 1)
            }
        }

        var imageUri: android.net.Uri? = null
        var outputStream: OutputStream? = null

        try {
            val resolver: ContentResolver = context.contentResolver
            imageUri = resolver.insert(imageCollectionUri, contentValues)
            if (imageUri == null) {
                throw IOException("Failed to create new MediaStore record")
            }

            outputStream = resolver.openOutputStream(imageUri)
            if (outputStream == null) {
                throw IOException("Failed to get output stream for MediaStore URI")
            }

            if (!image.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)) {
                throw IOException("Failed to save bitmap to MediaStore")
            }

            outputStream.flush()
            outputStream.close()

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                contentValues.clear()
                contentValues.put(MediaStore.Images.Media.IS_PENDING, 0)
                resolver.update(imageUri, contentValues, null, null)
            }

            // Notify the system to scan the saved image
            MediaScannerConnection.scanFile(
                context,
                arrayOf(imageUri.toString()),
                null,
                null
            )

            return@withContext true
        } catch (e: IOException) {
            // Handle any exception that occurred during the save process
            Toast.makeText(context, "Image failed to save", Toast.LENGTH_SHORT)
                .show()
            e.printStackTrace()
            if (imageUri != null) {
                context.contentResolver.delete(imageUri, null, null)
            }
            return@withContext false
        } finally {
            outputStream?.close()
        }
    }
}

fun getMediaPath(fileName: String): Uri? {
    val file = File("/path/to/file.jpg")
    val uri: Uri = Uri.fromFile(file)
    return uri
}

fun getImagesFromPicturesDirectory(
    contentResolver: ContentResolver,
    fileInfo: FileInfo
): ImageBitmap? {
    var imageBitmap: ImageBitmap? = null

    val projection = arrayOf(
        MediaStore.Images.Media._ID,
        MediaStore.Images.Media.DATA
    )
    val selection = "${MediaStore.Images.Media.DATA} like ?"
    val selectionArgs = arrayOf("%/Pictures/${fileInfo.fileName}%") // Filter by directory

    contentResolver.query(
        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
        projection,
        selection,
        selectionArgs,
        null
    )?.use { cursor ->
        val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
        val dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)

        imageBitmap = try {
            val uri = cursor.getString(dataColumn)
            loadImageBitmap(contentResolver, uri)
        } catch (e: Exception) {
            null
        }
//        while (cursor.moveToNext()) {
//            val id = cursor.getLong(idColumn)
//            val uri = cursor.getString(dataColumn)
//            images.add(MediaImage(id, uri))
//        }

    }

    return imageBitmap
}

private fun loadImageBitmap(contentResolver: ContentResolver, uri: String): ImageBitmap? {
    val inputStream = contentResolver.openInputStream(Uri.parse(uri))
    return inputStream?.use {
        BitmapFactory.decodeStream(it)?.asImageBitmap()
    }
}
